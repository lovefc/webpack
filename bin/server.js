/*
 * @Author       : lovefc
 * @Date         : 2021-12-20 15:19:31
 * @LastEditTime : 2021-12-20 15:19:31
 */
const path = require("path"); 
const connect = require("connect");
const serveStatic = require("serve-static");
let app = connect();
let dir = path.resolve(__dirname, '..'); // 获取上级目录
app.use(serveStatic(dir+"/dist/"));
app.listen(3000, '0.0.0.0',() => {
  console.log('静态服务器启动: http://localhost:3000/ ...');
});